package com.exairie.logtrackerclient.api

import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Exairie on 4/8/2018.
 */
interface ApiAccessInterface {
    @GET("schedule")
    fun getSchedules(@Header("Authorization") token : String) : Call<Array<ApiData.SalesSchedule>>

    @POST("schedule")
    fun saveSchedule(@Header("Authorization") token : String, @Body data : ApiData.SalesSchedule) : Call<Any>

    @PUT("schedule/{id}")
    fun updateSchedule(@Header("Authorization") token : String, @Path("id") id : Long, @Body data : ApiData.SalesSchedule) : Call<ApiData.SalesSchedule>

    @POST("location")
    fun sendLocation(@Header("Authorization") token : String, @Body data : ApiData.SalesLocation) : Call<Any>

    @POST("sales")
    fun register(@Body data : ApiData.Sales) : Call<Any>

    @POST("sales/login")
    fun login(@Body data : ApiData.Login) : Call<ApiData.Sales>
}