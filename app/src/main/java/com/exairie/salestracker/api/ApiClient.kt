package com.exairie.logtrackerclient.api


import com.exairie.salestracker.Configs
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder


/**
 * Created by Exairie on 4/8/2018.
 */
class ApiClient{
    companion object {
        val baseUrl = Configs.SERVER_UL + "/api/"
        var retrofit : Retrofit? = null

        fun getClient() : Retrofit{
            if(retrofit == null){
                val gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create()
                retrofit = Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
            }

            return retrofit!!
        }
    }



}