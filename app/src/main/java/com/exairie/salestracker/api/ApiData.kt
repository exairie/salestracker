package com.exairie.logtrackerclient.api

import android.content.ContentValues
import android.database.Cursor
import java.util.*

/**
 * Created by Exairie on 4/8/2018.
 */
class ApiData {
    data class Sales(
            val id : Long,
            var name : String?,
            var username : String?,
            var password : String?,
            var email : String?,
            var phone : String?,
            var token : String?,
            var created_at : Date,
            var updated_at : Date

    )
    data class SalesLocation(
            val sales_id : Long,
            val lat : Double,
            val lng : Double
    )
    data class SalesSchedule(
            val id : Long,
            val sales_id : Long,
            var schedule_title : String?,
            var schedule_desc : String?,
            var lat : Double,
            var lng : Double,
            var due_date : Long,
            var status : String,
            var created_at: Date,
            var updated_at: Date
    )
    data class Login(
            val username : String,
            val password : String
    )
}