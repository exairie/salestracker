package com.exairie.salestracker

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.*
import android.location.LocationManager.GPS_PROVIDER
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_map_select.*
import java.util.jar.Manifest
import android.os.AsyncTask
import java.io.IOException
import java.util.*
import android.widget.Toast




class ActivityMapSelect : AppCompatActivity() {
    lateinit var mMap : SupportMapFragment
    var googleMap : GoogleMap? = null
    var selectorMarker : Marker? = null
    var latLng = Location("")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_select)

        setSupportActionBar(toolbar)
        title = "Pick location"


        mMap = map as SupportMapFragment

        mMap.getMapAsync(object : OnMapReadyCallback{
            override fun onMapReady(p0: GoogleMap?) {
                googleMap = p0
                setupMap()
            }
        })

        setResult(0)
        btn_save_location.setOnClickListener({
            finish()
        })
    }

    fun geoCode(lat : Double, lng : Double){
        val context : Context? = this
        val task = object : AsyncTask<Void,Void,List<Address>?>(){
            override fun doInBackground(vararg params: Void?): List<Address>? {
                var addresses : List<Address>? = null
                val geocoder = Geocoder(context,Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(lat,lng,3)

                    if(addresses.size > 0){
                        val address = addresses[0]
                        var strAddress = ""
                        strAddress += address.adminArea + ", " +  address.thoroughfare + ", " + address.countryName

                        if(context!= null){
                            runOnUiThread {
                                location_text.setText(strAddress)
                            }
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                return null
            }

        }
        task.execute()

    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            1 -> {
                if(grantResults.size == 2 && grantResults[0] == PERMISSION_GRANTED && grantResults[1] == PERMISSION_GRANTED ){
                    setupMap()
                }
            }
            1 -> {
                if(grantResults.size == 2 && grantResults[0] == PERMISSION_GRANTED && grantResults[1] == PERMISSION_GRANTED ){
                    setupLocationUpdater()
                }
            }


        }
    }
    private fun installPermission() : Boolean {
        if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,arrayOf(ACCESS_FINE_LOCATION,ACCESS_COARSE_LOCATION),1)
            return false
        }
        return true
    }
    fun setupLocationUpdater(){
        if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,arrayOf(ACCESS_FINE_LOCATION,ACCESS_COARSE_LOCATION),2)
            return
        }
        val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if(manager.allProviders.contains(GPS_PROVIDER)){
            manager.requestLocationUpdates(GPS_PROVIDER,1000,100f,object : LocationListener{
                override fun onLocationChanged(location: Location?) {
                    val latLng = LatLng(location?.latitude ?: 0.0,location?.longitude ?: 0.0)
                    googleMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                    if(selectorMarker?.tag == 0){
                        selectorMarker?.position = latLng
                    }
                }

                override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?)  = Unit

                override fun onProviderEnabled(provider: String?)  = Unit

                override fun onProviderDisabled(provider: String?) = Unit
            })
        }
    }
    fun applyResult(){
        val ir = Intent()
        ir.putExtra("lat",latLng.latitude)
        ir.putExtra("lng",latLng.longitude)
        setResult(1,ir)
    }
    private fun setupMap() {
        if(ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,arrayOf(ACCESS_FINE_LOCATION,ACCESS_COARSE_LOCATION),1)
            return
        }
        googleMap?.isMyLocationEnabled = true

        selectorMarker = googleMap?.addMarker(MarkerOptions().position(LatLng(7.7956,110.3695)))
        selectorMarker?.isDraggable = true
        selectorMarker?.tag = 0
        googleMap?.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener{
            override fun onMarkerDragEnd(p0: Marker?) {
                latLng.latitude = p0?.position?.latitude?:7.7956
                latLng.longitude = p0?.position?.longitude ?: 110.3695
                geoCode(latLng.latitude,latLng.longitude)
                applyResult()
            }

            override fun onMarkerDragStart(p0: Marker?) = Unit

            override fun onMarkerDrag(p0: Marker?) = Unit
        })
    }
}
