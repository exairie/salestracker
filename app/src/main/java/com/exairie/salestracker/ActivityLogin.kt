package com.exairie.salestracker

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.exairie.logtrackerclient.api.ApiAccessInterface
import com.exairie.logtrackerclient.api.ApiClient
import com.exairie.logtrackerclient.api.ApiData
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivityLogin : AppCompatActivity() {
    lateinit var api : ApiAccessInterface
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        api = ApiClient.getClient().create(ApiAccessInterface::class.java)

        val pref = getSharedPreferences("com.exairie.salestracker", Context.MODE_PRIVATE)
        val udata = pref.getString("userdata","null")
        if(udata != "null"){
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
        btn_login.setOnClickListener({
            login()
        })
        btn_register.setOnClickListener({
            val i = Intent(this,ActivitySignUp::class.java)
            startActivityForResult(i,1)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1){
            if(resultCode == 1){
                Toast.makeText(this, "Please login with the registered username and password",Toast.LENGTH_LONG).show()
            }
        }
    }
    fun login(){
        val progress = ProgressDialog(this)
        progress.setMessage("Logging In")
        progress.setCancelable(false)
        progress.show()
        val data = ApiData.Login(t_username.text.toString(),t_password.text.toString())

        api.login(data).enqueue(object : Callback<ApiData.Sales>{
            override fun onFailure(call: Call<ApiData.Sales>?, t: Throwable?) {
                runOnUiThread {
                    progress.dismiss()
                    AlertDialog.Builder(this@ActivityLogin)
                            .setMessage("Invalid Login!")
                            .setTitle("Login Failed")
                            .setNegativeButton("Close",{d,_-> d.dismiss()})
                            .show()
                }
                t?.printStackTrace()
            }

            override fun onResponse(call: Call<ApiData.Sales>?, response: Response<ApiData.Sales>?) {

                progress.dismiss()
                if(response?.code() == 404){
                    runOnUiThread {
                        AlertDialog.Builder(this@ActivityLogin)
                                .setMessage("Invalid Login!")
                                .setTitle("Login Failed")
                                .setNegativeButton("Close",{d,_-> d.dismiss()})
                                .show()
                    }
                }
                if(response?.code() == 200){
                    val pref = getSharedPreferences("com.exairie.salestracker", Context.MODE_PRIVATE)
                    with(pref.edit()){
                        putString("userdata",Gson().toJson(response.body()))
                        commit()
                    }

                    val i = Intent(this@ActivityLogin,MainActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }
        })
    }

}
