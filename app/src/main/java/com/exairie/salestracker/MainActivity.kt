package com.exairie.salestracker

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.*
import android.widget.PopupMenu
import android.widget.Toast
import com.exairie.logtrackerclient.api.ApiAccessInterface
import com.exairie.logtrackerclient.api.ApiClient
import com.exairie.logtrackerclient.api.ApiData
import com.exairie.salestracker.services.LocationService
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_schedule.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    lateinit var api : ApiAccessInterface
    lateinit var user : ApiData.Sales
    val adapter : Adapter = Adapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        api = ApiClient.getClient().create(ApiAccessInterface::class.java)

        setSupportActionBar(toolbar)
        setTitle("Your Schedules")
        val pref = getSharedPreferences("com.exairie.salestracker", Context.MODE_PRIVATE)
        val udata = pref.getString("userdata","null")
        if(udata == "null"){
            finish()
        }
        user = Gson().fromJson(udata, ApiData.Sales::class.java)

        swipe_refresh.setOnRefreshListener {
            loadData()
        }
        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(this)

        val i = Intent(this,LocationService::class.java)
        startService(i)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.mn_new_schedule->{
                val i = Intent(this,ActivityInputSchedule::class.java)
                startActivityForResult(i,1)
            }
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            1->{
                if(resultCode == 1){
                    loadData()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        loadData()
    }
    fun loadData(){
        swipe_refresh.isRefreshing = true
        api.getSchedules(user.token ?: "").enqueue(object : Callback<Array<ApiData.SalesSchedule>>{
            override fun onFailure(call: Call<Array<ApiData.SalesSchedule>>?, t: Throwable?) {
                runOnUiThread {
                    swipe_refresh.setRefreshing(false)
                }
                Toast.makeText(this@MainActivity,"Unable to get schedules from server",Toast.LENGTH_LONG).show()
                t?.printStackTrace()
            }

            override fun onResponse(call: Call<Array<ApiData.SalesSchedule>>?, response: Response<Array<ApiData.SalesSchedule>>?) {
                runOnUiThread {
                    swipe_refresh.setRefreshing(false)
                }
                Log.d("BODY",response?.body().toString())
                if(response?.code() == 403){
                    Toast.makeText(this@MainActivity,"Unable to get schedules from server. Invalid Credential",Toast.LENGTH_LONG).show()
                }
                if(response?.code() == 200){
                    adapter.data = response.body()
                    recycler.post({
                        adapter.notifyDataSetChanged()
                    })
                }
            }
        })
    }
    inner class Adapter : RecyclerView.Adapter<Holder>() {
        var data : Array<ApiData.SalesSchedule>? = arrayOf()
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_schedule,parent,false)
            return Holder(v)
        }

        override fun getItemCount(): Int {
            return data?.size ?: 0
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val cData = data?.get(position)
            if(cData != null){
                holder.itemView.l_title.setText(cData.schedule_title)
                holder.itemView.l_title.setText(cData.schedule_desc)
                holder.itemView.l_due_date.setText("Due : ${DateFormat.format("dd-MM-yyyy",cData.due_date)}")
                holder.itemView.l_status.setText(cData.status)

                holder.itemView.setOnLongClickListener({vw->
                    val popup = PopupMenu(this@MainActivity,vw)
                    menuInflater.inflate(R.menu.menu_schedule,popup.menu)
                    popup.setOnMenuItemClickListener { menuitem ->
                        when(menuitem.itemId){
                            R.id.mn_finish->{
                                finishSchedule(cData)
                            }
                        }
                        return@setOnMenuItemClickListener true
                    }
                    popup.show()
                    return@setOnLongClickListener true
                })
            }

        }
    }

    private fun finishSchedule(cData: ApiData.SalesSchedule) {
        cData.status = "Finished"
        api.updateSchedule(user.token ?: "",cData.id,cData).enqueue(object : Callback<ApiData.SalesSchedule>{
            override fun onFailure(call: Call<ApiData.SalesSchedule>?, t: Throwable?) {
                t?.printStackTrace()
            }

            override fun onResponse(call: Call<ApiData.SalesSchedule>?, response: Response<ApiData.SalesSchedule>?) {
                loadData()
                sendBroadcast(Intent(LocationService.NOTIFIER))
            }
        })
    }

    class Holder(v : View) : RecyclerView.ViewHolder(v)

}
