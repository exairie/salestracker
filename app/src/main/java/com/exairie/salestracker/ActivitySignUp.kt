package com.exairie.salestracker

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.exairie.logtrackerclient.api.ApiAccessInterface
import com.exairie.logtrackerclient.api.ApiClient
import com.exairie.logtrackerclient.api.ApiData

import kotlinx.android.synthetic.main.activity_sign_up.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ActivitySignUp : AppCompatActivity() {

    lateinit var api : ApiAccessInterface
    var inProcess = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        api = ApiClient.getClient().create(ApiAccessInterface::class.java)
        setResult(0)

        btn_register.setOnClickListener({
            if(!inProcess){
                signUp()
            }
        })
    }
    fun signUp(){
        inProcess = true
        txt_btn_sign_up.text = "Signing Up..."
        val data = ApiData.Sales(-1,t_name.text.toString(),
                t_username.text.toString(),
                t_password.text.toString(),
                t_email.text.toString(),
                t_phone.text.toString(),null, Date(),Date())
        api.register(data).enqueue(object : Callback<Any>{
            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                Toast.makeText(this@ActivitySignUp,"Register failed!",Toast.LENGTH_LONG).show()
                runOnUiThread {
                    txt_btn_sign_up.text = "Sign Up"
                }
                inProcess = false
                t?.printStackTrace()
            }

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                if(response?.code() == 201){
                    Toast.makeText(this@ActivitySignUp,"Register Success!",Toast.LENGTH_SHORT).show()
                    setResult(1)
                    finish()
                }else{
                    inProcess = false
                    Toast.makeText(this@ActivitySignUp,"Register failed!",Toast.LENGTH_LONG).show()
                }
            }
        })
    }

}
