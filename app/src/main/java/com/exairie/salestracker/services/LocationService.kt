package com.exairie.salestracker.services

import android.annotation.SuppressLint
import android.app.Notification
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.text.format.DateFormat
import android.util.Log
import com.exairie.logtrackerclient.api.ApiAccessInterface
import com.exairie.logtrackerclient.api.ApiClient
import com.exairie.logtrackerclient.api.ApiData
import com.exairie.salestracker.R
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationService : Service() {
    val receiver = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            reInitSchedule()
        }
    }
    companion object {
        val NOTIFIER = "com.exairie.salestracker.NOTIFY_SCHEDULE_CHANGED"
        var installed = false
    }
    lateinit var api : ApiAccessInterface
    private var user: ApiData.Sales? = null

    @SuppressLint("MissingPermission")
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        api = ApiClient.getClient().create(ApiAccessInterface::class.java)
        val pref = getSharedPreferences("com.exairie.salestracker", Context.MODE_PRIVATE)
        val udata = pref.getString("userdata","null")
        if(udata == "null"){
            return START_STICKY
        }
        user = Gson().fromJson(udata, ApiData.Sales::class.java)
        val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if(manager.allProviders.contains(GPS_PROVIDER)){
            manager.requestLocationUpdates(GPS_PROVIDER,1000 * 60 * 30,0f,object : LocationListener{
                override fun onLocationChanged(location: Location?) {
                    val loc = ApiData.SalesLocation(user?.id ?: -1,location?.latitude ?: 0.0,location?.longitude ?: 0.0)
                    api.sendLocation(user?.token ?: "",loc).enqueue(object : Callback<Any> {
                        override fun onFailure(call: Call<Any>?, t: Throwable?) {
                        }

                        override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                            Log.d("SEND OK","SEND OK OK OK OK ")
                        }
                    })
                }

                override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) = Unit

                override fun onProviderEnabled(provider: String?) = Unit

                override fun onProviderDisabled(provider: String?) = Unit
            })
        }

        val notification = Notification.Builder(this)
        notification.setContentTitle("Sales Tracker")
        notification.setContentText("Your progress is active")
        notification.setLargeIcon(BitmapFactory.decodeResource(resources,R.mipmap.ic_launcher))
        notification.setSmallIcon(R.drawable.ic_trending_up_white_48dp)

        startForeground(109,notification.build())

        try {
            registerReceiver(receiver, IntentFilter(NOTIFIER))
        }catch (e : Exception){
            e.printStackTrace()
        }

        reInitSchedule()

        return START_STICKY
    }

    override fun onDestroy() {
        try {
            unregisterReceiver(receiver)
        }catch (e : Exception){
            e.printStackTrace()
        }
        super.onDestroy()
    }
    override fun onBind(intent: Intent): IBinder? = null

    fun reInitSchedule(){
        api?.let {
            api.getSchedules(user?.token ?: "").enqueue(object : Callback<Array<ApiData.SalesSchedule>>{
                override fun onFailure(call: Call<Array<ApiData.SalesSchedule>>?, t: Throwable?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onResponse(call: Call<Array<ApiData.SalesSchedule>>?, response: Response<Array<ApiData.SalesSchedule>>?) {
                    response?.body()?.let {
                        if((response.body()?.count() ?: 0) < 1){
                            val notification = Notification.Builder(this@LocationService)
                            notification.setContentTitle("Sales Tracker")
                            notification.setContentText("Your have 0 task scheduled")
                            notification.setLargeIcon(BitmapFactory.decodeResource(resources,R.mipmap.ic_launcher))
                            notification.setSmallIcon(R.drawable.ic_trending_up_white_48dp)

                            startForeground(109,notification.build())
                        }else{
                            response.body()?.sortBy { sch -> sch.due_date }
                            response.body()?.let {
                                val schedule = response.body()?.first()
                                val notification = Notification.Builder(this@LocationService)
                                notification.setContentTitle("Sales Tracker")
                                notification.setContentText("You're scheduled for ${schedule?.schedule_title} at ${if(schedule?.due_date != null)DateFormat.format("dd-MM-yyyy",schedule.due_date)else "-"}")
                                notification.setLargeIcon(BitmapFactory.decodeResource(resources,R.mipmap.ic_launcher))
                                notification.setSmallIcon(R.drawable.ic_trending_up_white_48dp)

                                startForeground(109,notification.build())
                            }
                        }
                    }
                }
            })
        }
    }
}
