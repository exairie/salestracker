package com.exairie.salestracker

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.exairie.logtrackerclient.api.ApiAccessInterface
import com.exairie.logtrackerclient.api.ApiClient
import com.exairie.logtrackerclient.api.ApiData
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_input_schedule.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.util.*

class ActivityInputSchedule : AppCompatActivity() {
    var prevData : ApiData.SalesSchedule? = null
    private var inProcess: Boolean = false
    lateinit var user : ApiData.Sales
    lateinit var api : ApiAccessInterface
    var lat : Double = 0.0
    var lng : Double = 0.0
    var due : Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_schedule)

        setSupportActionBar(toolbar)
        title = "Input Schedule"

        api = ApiClient.getClient().create(ApiAccessInterface::class.java)

        val pref = getSharedPreferences("com.exairie.salestracker", Context.MODE_PRIVATE)

        val udata = pref.getString("userdata","null")
        if(udata == "null"){
            finish()
        }


        user = Gson().fromJson(udata, ApiData.Sales::class.java)


        val pData = intent.getStringExtra("data")
        if(pData != null){
            prevData = Gson().fromJson(pData,ApiData.SalesSchedule::class.java)
            populatePrevData()
        }
        setResult(0)
        val now = Calendar.getInstance()
        t_duedate.setOnFocusChangeListener({v,focus->
            if(focus){
                val picker = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(object : com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener{
                    override fun onDateSet(p0: com.fourmob.datetimepicker.date.DatePickerDialog?, p1: Int, p2: Int, p3: Int) {
                        val selected = Calendar.getInstance()
                        selected.set(p1,p2,p3,0,0,0)
                        due = selected.timeInMillis
                        t_duedate.setText(android.text.format.DateFormat.format("dd-MM-yyyy",due))
                    }
                },now.get(Calendar.YEAR),now.get(Calendar.MONTH),now.get(Calendar.DAY_OF_MONTH))
                picker.show(supportFragmentManager,"DATEPICKER")
            }
        })

        btn_openmap.setOnClickListener({
            val i = Intent(this,ActivityMapSelect::class.java)
            startActivityForResult(i,9)
        })

        btn_save.setOnClickListener({
            if(prevData != null){
                save()
            }
            else{
                create()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            9->{
                lat = intent?.getDoubleExtra("lat",7.7956) ?: 7.7956
                lng = intent?.getDoubleExtra("lng",110.3695) ?: 110.3695
            }
        }
    }
    fun populatePrevData(){
        t_title.setText(prevData?.schedule_title)
        t_desc.setText(prevData?.schedule_desc)
        t_duedate.setText(android.text.format.DateFormat.format("dd-mm-yyyy",prevData?.due_date?:0))
    }

    fun save(){
        inProcess = true
        val data = ApiData.SalesSchedule(
                prevData?.id ?: -1,user.id,t_title.text.toString(),
                t_desc.text.toString(),
                lat,lng,
                due,
                "Created",
                Date(),Date()

        )
        api.updateSchedule(user.token ?: "",prevData?.id ?: -1,data).enqueue(object : Callback<ApiData.SalesSchedule>{
            override fun onFailure(call: Call<ApiData.SalesSchedule>?, t: Throwable?) {
                Toast.makeText(this@ActivityInputSchedule,"Update failed!",Toast.LENGTH_LONG).show()
                runOnUiThread {
                }
                inProcess = false
                t?.printStackTrace()
            }

            override fun onResponse(call: Call<ApiData.SalesSchedule>?, response: Response<ApiData.SalesSchedule>?) {
                Toast.makeText(this@ActivityInputSchedule,"Update Success!",Toast.LENGTH_SHORT).show()
                setResult(1)
                finish()
            }
        })
    }
    fun create(){
        inProcess = true
        val data = ApiData.SalesSchedule(
                -1,user.id,t_title.text.toString(),
                t_desc.text.toString(),
                lat,lng,
                due,
                "Created",
                Date(),Date()

        )
        api.saveSchedule(user.token ?: "",data).enqueue(object : Callback<Any>{
            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                Toast.makeText(this@ActivityInputSchedule,"Save failed!",Toast.LENGTH_LONG).show()
                runOnUiThread {
                }
                inProcess = false
                t?.printStackTrace()
            }

            override fun onResponse(call: Call<Any>?, response: Response<Any>?) {
                Toast.makeText(this@ActivityInputSchedule,"Save Success!",Toast.LENGTH_SHORT).show()
                setResult(1)
                finish()
            }
        })
    }


}
